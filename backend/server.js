const express = require("express"); const bodyParser = require("body-parser");  const cors = require("cors");
const mongoUtils = require("./db/utils");   const { directorModel,filmModel } = require('./model/model');
const app=express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
mongoUtils.mongooseConnect();
// mongoUtils.Connect();

app.get("/directors",(req,res)=>{
    directorModel.find((e,r)=>{
        if(e) { console.log(e); }
        return res.send(r);
    })
})

app.post("/directors",(req,res)=>{
    if(req.body){
        const s=new directorModel(req.body);
        s.save().then(r => {return res.send(r);}).catch(e =>console.log(e));
    }
})
app.get("/directors/:name" , (req,res)=>{
    if(req.params) {
        var name=req.params.name;
        directorModel.findOne({ name }).then(r=>{return res.send(r);}).catch(e=>console.log(e));
    }
})
app.patch("/directors/:name" , (req,res)=>{
    if(req.params) {
        var name=req.params.name;
        directorModel.findOneAndUpdate({name},{$set:req.body}).then(r=>{return res.send(r);}).catch(e=>console.log(e));
    }
})
app.delete("/directors/:name" , (req,res)=>{
    if(req.params) {
        var name=req.params.name;
        directorModel.deleteOne({ name }).then(r=>{return res.send(r);}).catch(e=>console.log(e));
    }
})
app.listen(8080,()=>{
    console.log("connected")
})


