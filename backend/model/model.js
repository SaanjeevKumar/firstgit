const mongoose=require("mongoose");
const director=mongoose.Schema({
    name:{
        type:String,
        requried:true,
    },
    age:{
        type:Number,
        required:true,
    },
    gender:{
        type:String,
        required:true,
    },
    awards:{
        type:Number,
        required:true,
    },
})



const directorModel=mongoose.model("director",director);


module.exports={directorModel};
